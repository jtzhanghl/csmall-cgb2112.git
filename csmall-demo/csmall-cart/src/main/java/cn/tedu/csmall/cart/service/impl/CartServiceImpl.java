package cn.tedu.csmall.cart.service.impl;


import cn.tedu.csmall.cart.mapper.CartMapper;
import cn.tedu.csmall.cart.service.ICartService;
import cn.tedu.csmall.commons.pojo.cart.dto.CartAddDTO;
import cn.tedu.csmall.commons.pojo.cart.model.CartTb;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class CartServiceImpl implements ICartService {
    @Autowired
    private CartMapper cartMapper;
    @Override
    public void cartAdd(CartAddDTO cartAddDTO) {
        log.info("控制层调用新增购物车--开始");
        CartTb cart=new CartTb();
        //转化model
        BeanUtils.copyProperties(cartAddDTO,cart);
        //新增入库
        cartMapper.insertCart(cart);
        log.info("新增购物车经过转化入库--结束");
    }

    @Override
    public void deleteUserCart(String userId, String commodityCode) {
        log.info("控制层调用删除用户购物车--开始");
        cartMapper.deleteCartByUserIdAndCommodityCode(userId,commodityCode);
        log.info("删除用户购物车入库--结束");
    }
}
