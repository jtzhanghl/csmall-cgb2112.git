package cn.tedu.csmall.cart.controller;


import cn.tedu.csmall.cart.service.ICartService;
import cn.tedu.csmall.commons.pojo.cart.dto.CartAddDTO;
import cn.tedu.csmall.commons.restful.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/base/cart")
@Api(tags="购物车模块")
public class CartController {

    @Autowired
    private ICartService cartService;

    /**
     * 新增购物车
     * @param cartAddDTO
     * @return 成功失败消息
     */
    @ApiOperation("新增购物车")
    @PostMapping("/add")
    public JsonResult cartAdd(CartAddDTO cartAddDTO){
        cartService.cartAdd(cartAddDTO);
        return JsonResult.ok("新增购物车成功");
    }
    @ApiOperation("删除已经提交订单的购物车")
    @PostMapping("/delete")
    @ApiImplicitParams({
        @ApiImplicitParam(value="用户ID",name="userId",example = "UU100",required = true),
        @ApiImplicitParam(value="商品编号",name="commodityCode",example = "PC100",required = true)
    })
    public JsonResult deleteUserCart(String userId,String commodityCode){
        cartService.deleteUserCart(userId,commodityCode);
        return JsonResult.ok("删除用户购物车商品成功");
    }
}
