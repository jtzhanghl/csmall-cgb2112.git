package cn.tedu.csmall.cart;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CsmallCartApplication {

    public static void main(String[] args) {
        SpringApplication.run(CsmallCartApplication.class, args);
    }

}
