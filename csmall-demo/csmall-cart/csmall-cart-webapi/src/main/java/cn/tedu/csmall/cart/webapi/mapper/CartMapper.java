package cn.tedu.csmall.cart.webapi.mapper;


import cn.tedu.csmall.commons.pojo.cart.model.CartTb;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CartMapper {
    @Insert("insert into cart_tbl (commodity_code,user_id,price,count) " +
            "values (#{commodityCode},#{userId},#{price},#{count})")
    void insertCart(CartTb cart);
    @Delete("delete from cart_tbl where user_id=#{userId} and commodity_code=#{commodityCode}")
    void deleteCartByUserIdAndCommodityCode(@Param("userId") String userId, @Param("commodityCode") String commodityCode);
}
