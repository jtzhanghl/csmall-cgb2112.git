package cn.tedu.csmall.stock.webapi.controller;

import cn.tedu.csmall.commons.pojo.stock.dto.StockReduceCountDTO;
import cn.tedu.csmall.commons.restful.JsonResult;
import cn.tedu.csmall.stock.service.IStockService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Api(tags = "库存模块")
@RequestMapping("/base/stock")
public class StockController {
    @Autowired
    private IStockService stockService;
    @ApiOperation("商品减库存")
    @PostMapping("/reduce/count")
    public JsonResult reduceCommodityCount(StockReduceCountDTO stockReduceCountDTO){
        stockService.reduceCommodityCount(stockReduceCountDTO);
        return JsonResult.ok("商品扣除库存成功");
    }
    @ApiOperation("测试")
    @GetMapping("/reduce/demo")
    public JsonResult reduceDemo(){
        System.out.println("hellow!!!!");
        return JsonResult.ok("hellowwww");
    }
}
