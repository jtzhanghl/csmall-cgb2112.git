package cn.tedu.csmall.stock.webapi.mapper;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

@Repository
public interface StockMapper {
    @Update("update stock_tbl " +
            "set count=count-#{reduceCount} " +
            "where count>#{reduceCount} " +
            "and " +
            "commodity_code=#{commodityCode}")
    void updateStockCountByCommodityCoud(@Param("commodityCode") String commodityCode, @Param("reduceCount") Integer reduceCount);
}
