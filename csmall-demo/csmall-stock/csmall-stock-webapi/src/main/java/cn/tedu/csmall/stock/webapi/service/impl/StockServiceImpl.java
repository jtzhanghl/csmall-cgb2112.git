package cn.tedu.csmall.stock.webapi.service.impl;


import cn.tedu.csmall.commons.pojo.stock.dto.StockReduceCountDTO;
import cn.tedu.csmall.stock.service.IStockService;
import cn.tedu.csmall.stock.webapi.mapper.StockMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@DubboService
@Service
@Slf4j
public class StockServiceImpl implements IStockService {
    @Autowired
    private StockMapper stockMapper;
    @Override
    public void reduceCommodityCount(StockReduceCountDTO stockReduceCountDTO) {
        log.info("控制层调用减少商品库存--开始");
        stockMapper.updateStockCountByCommodityCoud(stockReduceCountDTO.getCommodityCode(),stockReduceCountDTO.getReduceCount());
        log.info("商品减库存入库--结束");
    }
}
