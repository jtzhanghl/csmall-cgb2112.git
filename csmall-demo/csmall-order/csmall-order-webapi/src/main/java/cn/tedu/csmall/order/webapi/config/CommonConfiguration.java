package cn.tedu.csmall.order.webapi.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"cn.tedu.csmall.commons.exception.handler"})
public class CommonConfiguration {
}
