package cn.tedu.csmall.order.webapi.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

/**
 * 只做包扫描
 * 如果不需要mybatis配置可以注释掉
 */
@Configuration
@MapperScan("cn.tedu.csmall.order.webapi.mapper")
public class MyBatisConfiguration {
}
