package cn.tedu.csmall.order.service;

import cn.tedu.csmall.commons.pojo.order.dto.OrderAddDTO;

public interface IOrderService {
    void orderAdd(OrderAddDTO orderAddDTO);
}
