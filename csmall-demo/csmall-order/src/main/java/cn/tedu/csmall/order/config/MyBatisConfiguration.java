package cn.tedu.csmall.order.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

/**
 * 只做包扫描
 * 如果不需要mybatis配置可以注释掉
 */
@Configuration
@MapperScan("cn.tedu.csmall.order.mapper")
public class MyBatisConfiguration {
}
