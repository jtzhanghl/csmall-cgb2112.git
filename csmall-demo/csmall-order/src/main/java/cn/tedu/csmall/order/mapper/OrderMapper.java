package cn.tedu.csmall.order.mapper;


import cn.tedu.csmall.commons.pojo.order.model.OrderTb;
import org.apache.ibatis.annotations.Insert;

public interface OrderMapper {
    @Insert("insert into order_tbl " +
            "(user_id,commodity_code,count,money) " +
            "values " +
            "(#{userId},#{commodityCode},#{count},#{money})")
    void insertOrder(OrderTb order);
}
