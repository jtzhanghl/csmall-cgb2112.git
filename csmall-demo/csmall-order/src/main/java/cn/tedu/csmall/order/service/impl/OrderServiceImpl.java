package cn.tedu.csmall.order.service.impl;


import cn.tedu.csmall.commons.pojo.order.dto.OrderAddDTO;
import cn.tedu.csmall.commons.pojo.order.model.OrderTb;
import cn.tedu.csmall.order.mapper.OrderMapper;
import cn.tedu.csmall.order.service.IOrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class OrderServiceImpl implements IOrderService {
    @Autowired
    private OrderMapper orderMapper;
    @Override
    public void orderAdd(OrderAddDTO orderAddDTO) {
        log.info("控制层调用新增订单--开始");
        log.info("对商品:{},进行减库存:{}个操作--TODO",orderAddDTO.getCommodityCode(),orderAddDTO.getCount());
        log.info("删除用户:{}购买的购物车商品:{}操作--TODO",orderAddDTO.getUserId(),orderAddDTO.getCommodityCode());
        OrderTb order=new OrderTb();
        BeanUtils.copyProperties(orderAddDTO,order);
        orderMapper.insertOrder(order);
        log.info("新增订单入库--结束");
    }
}
