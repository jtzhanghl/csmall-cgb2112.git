package cn.tedu.csmall.order;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CsmallOrderApplication {

    public static void main(String[] args) {
        SpringApplication.run(CsmallOrderApplication.class, args);
    }

}
