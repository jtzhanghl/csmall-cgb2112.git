package cn.tedu.csmall.business.controller;

import cn.tedu.csmall.business.service.IBusinessService;
import cn.tedu.csmall.commons.restful.JsonResult;
import cn.tedu.csmall.commons.restful.ResponseCode;
import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/base/business")
@Api(tags = "购买业务模块")
public class BusinessController {
    @Autowired
    private IBusinessService businessService;
    /**
     * 添加限流 降级服务
     * @return
     */
    @ApiOperation("发起购买")
    @PostMapping("/buy")
    @SentinelResource(value="buy",blockHandler = "blockError",fallback = "fallbackError")
    public JsonResult buy(){
        businessService.buy();
        return JsonResult.ok("购买成功");
    }
    public JsonResult blockError(BlockException e){
        return JsonResult.failed(ResponseCode.BAD_REQUEST,"流量控制,当前购买人数太多,请稍后操作");
    }
    public JsonResult fallbackError(Throwable e){
        return JsonResult.failed(ResponseCode.BAD_REQUEST,"添加订单失败,请重新提交");
    }
}
