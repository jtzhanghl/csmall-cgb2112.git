package cn.tedu.csmall.business.service.impl;


import cn.tedu.csmall.business.service.IBusinessService;
import cn.tedu.csmall.commons.exception.CoolSharkServiceException;
import cn.tedu.csmall.commons.pojo.order.dto.OrderAddDTO;
import cn.tedu.csmall.commons.restful.ResponseCode;
import cn.tedu.csmall.order.service.IOrderService;
import io.seata.spring.annotation.GlobalTransactional;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.math.RandomUtils;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class BusinessServiceImpl implements IBusinessService {
    @DubboReference
    private IOrderService dubboOrderService;
    @GlobalTransactional
    @Override
    public void buy() {
        OrderAddDTO orderAddDTO=new OrderAddDTO();
        orderAddDTO.setCommodityCode("PC100");
        orderAddDTO.setCount(5);
        orderAddDTO.setMoney(500);
        orderAddDTO.setUserId("UU100");
        log.info("支付金额:{}",orderAddDTO.getMoney());
        log.info("购买即将开始--远程调用");
        dubboOrderService.orderAdd(orderAddDTO);
        log.info("这里有可能出现异常,认为概率");
        if(RandomUtils.nextBoolean()){
            throw new CoolSharkServiceException(ResponseCode.INTERNAL_SERVER_ERROR,"人为异常测试回滚");
        }
    }
}
